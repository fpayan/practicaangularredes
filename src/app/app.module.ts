import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Modules
import { LoginModule } from './modules/login/login.module';
import { LoginRoutingModule } from './modules/login/login.routing.module';
//
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
//import { InfoAuthorComponent } from './shared/info-author/info-author.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LoginModule,
    LoginRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
