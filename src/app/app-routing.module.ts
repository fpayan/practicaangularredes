import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '../app/modules/login/login/login.component';
import { ForgotLoginComponent } from './modules/login/forgot-login/forgot-login.component';
import { RegisterLoginComponent } from './modules/login/register-login/register-login.component';


const routes: Routes = [
  // {
  //   path: '', component: LoginComponent,
  //   children: [
  //     { path: 'register', component: RegisterLoginComponent },
  //     { path: 'forgot', component: ForgotLoginComponent}
  //   ]
  // },
  // { path: 'register', component: RegisterLoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
