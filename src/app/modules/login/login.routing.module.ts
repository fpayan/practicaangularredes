import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ForgotLoginComponent } from './forgot-login/forgot-login.component';
import { RegisterLoginComponent } from './register-login/register-login.component';


const routes: Routes = [
  {
    path: '', component: LoginComponent,
    // children: [
    //   { path: 'register', component: RegisterLoginComponent },
    //   { path: 'forgot', component: ForgotLoginComponent}
    // ]
  },
  { path: 'register', component: RegisterLoginComponent },
  { path: 'forgot', component: ForgotLoginComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
