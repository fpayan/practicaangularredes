import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { environment } from '../../../environments/environment';
import { LoggerModule } from 'ngx-logger';
//
import { LoginComponent } from '../../modules/login/login/login.component';
import { RegisterLoginComponent } from '../../modules/login/register-login/register-login.component';
import { ForgotLoginComponent } from '../../modules/login/forgot-login/forgot-login.component';
import { InfoAuthorComponent } from '../../../app/shared/info-author/info-author.component';


@NgModule({
  declarations: [
    LoginComponent,
    RegisterLoginComponent,
    ForgotLoginComponent,
    InfoAuthorComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    LoggerModule.forRoot({
      level: environment.logLevel,
      serverLogLevel: environment.serverLogLevel,
      disableConsoleLogging: false
    })
  ],
  exports: [
    LoginComponent,
    RegisterLoginComponent,
    ForgotLoginComponent
  ]
})
export class LoginModule { }
