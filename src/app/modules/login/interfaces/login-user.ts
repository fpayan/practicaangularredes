export interface LoginUser {
  // Login of user
  email: string;
  password: string;
}
