import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoginUser } from '../interfaces/login-user';
import { NGXLogger } from 'ngx-logger';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  static readonly BASE_URL: string = 'https://reqres.in';


  constructor(
    private httpClient: HttpClient,
    private httpHeader: HttpHeaders,
    private logger: NGXLogger
    ) { }

  //POST
  registerUser(user: LoginUser, urlPart: string = '/api/register'){
    this.logger.info(`registerUser() : ${JSON.stringify(user)} it will be sent ${new Date()} `);
    return this.httpClient.post(LoginService.BASE_URL + urlPart, user).subscribe();
  }

  //POST
  loginAction(user: LoginUser, urlPart: string = '/api/login'){
    this.logger.info(`loginAction() : ${JSON.stringify(user)} it will be sent ${new Date()} `);
    return this.httpClient.post(LoginService.BASE_URL + urlPart, user).subscribe();
  }
}
